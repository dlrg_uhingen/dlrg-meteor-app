// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { permission, ucFirst } from '../../imports/functions.js';
import { Groups } from '../../imports/collections/groups.js';
import { Members } from '../../imports/collections/members.js';

Meteor.publish('groups', ()=>{
  if(Meteor.userId()){
    return Groups.find();
  }
});

Meteor.methods({
  'groups.add'(name) {
    if(!permission("addGroup")) return;
    check(name, String);
    if(!name){
      return "groupname is empty";
    }
    return Groups.insert({
      name: ucFirst(name),
      createdAt: new Date(),
    });
  },
  'groups.setName'(groupId,name) {
    if(!permission("editGroup")) return;
    check(name, String);
    return Groups.update(groupId,{ $set: {
      name
    } });
  },
  'groups.delete'(groupId) {
    if(!permission("deleteGroup")) return;
    check(groupId, String);
    Members.find().forEach((member)=>{
      if(member.groupIds){
        let index = member.groupIds.indexOf(groupId);
        if(index != -1){
          if(member.groupIds.length > 1){
            delete member.groupIds[index];
            Members.update(member._id,member);
          }else{
            Members.update(member._id,{
              $unset: { groupIds: 1 }
            });
          }
        }
      }
    });
    return Groups.remove(groupId);
  },
});
