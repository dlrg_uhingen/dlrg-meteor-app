// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Events } from '../../imports/collections/events.js';
import { Members } from '../../imports/collections/members.js';
import { permission } from '../../imports/functions.js';


Meteor.publish('self', ()=>{
  return Meteor.users.find(Meteor.userId(),{
    fields: {
      permission: 1,
    }
  });
});

Meteor.publish('users', ()=>{
  if(permission("editUser")){
    return Meteor.users.find({username: {$ne: "admin"}},{
      fields: {
        username: 1,
        permission: 1,
      }
    });
  }
});

function checkUsername(username,callback){
  username = username.trim();
  username = username.split(" ").join("_");
  if(!username){
    return "username_empty";
  }
  if(Meteor.users.findOne({username: username})){
    return "username_exists";
  }
  return callback(username);
}

Meteor.methods({
  'users.add'(username,password){
    if(permission("editUser")){
      check(username, String);
      check(password, String);
      return checkUsername(username,(checkedUsername)=>{
        return Accounts.createUser({
          username: checkedUsername,
          password
        });
      });
    }
  },
  'users.setUsername'(userId,username){
    if(permission("editUser") || userId == Meteor.userId()){
      check(username, String);
      check(userId, String);
      return checkUsername(username,(checkedUsername)=>{
        return Meteor.users.update(userId,{ $set: {
          username: checkedUsername
        } });
      });
    }
  },
  'users.setPermission'(userId,permissions){
    check(permissions, Array);
    check(userId, String);

    if(userId == Meteor.userId()){
      return "self_editing_not_allowed";
    }

    permissions.forEach((perm)=>{
      if(!permission(perm)){
        return "not_enought_permissions";
      }
    });

    if(permissions.length == 0){
      return Meteor.users.update(userId,{ $unset: {
        permission: 1
      } });
    }else{
      return Meteor.users.update(userId,{ $set: {
        permission:permissions
      } });
    }
  },
  'users.delete'(userId){
    if(permission("deleteUser") || userId == Meteor.userId()){
      check(userId, String);
      return Meteor.users.remove(userId);
    }
  }
});
