// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Events } from '../../imports/collections/events.js';
import { Members } from '../../imports/collections/members.js';
import { permission, ucFirst } from '../../imports/functions.js';


Meteor.publish('events', ()=>{
  if(Meteor.userId()){
    return Events.find();
  }
});

Meteor.methods({
  'events.add'(title,date) {
    if(!permission("addEvent")) return;
    check(title, String);
    check(date, Date);
    if(!title)
      return "title is invalid";
    if(!date)
      return "date is invalid";

    return Events.insert({
      title: ucFirst(title),
      date: date,
      createdAt: new Date(),
    });
  },
  'events.setTitle'(eventId,title) {
    if(!permission("editEvent")) return;
    check(eventId, String);
    check(title, String);
    if(!title)
      return "title is invalid";

    return Events.update(eventId,{ $set: {
      title
    } });
  },
  'events.setDate'(eventId,date) {
    if(!permission("editEvent")) return;
    check(eventId, String);
    check(date, Date);
    if(!date)
      return "date is invalid";

    return Events.update(eventId,{ $set: {
      date
    } });
  },
  'events.delete'(eventId) {
    if(!permission("deleteEvent")) return;
    check(eventId, String);
    Members.find().forEach((member)=>{
      if(member.attendence && member.attendence[eventId]){
        delete member.attendence[eventId];
        Members.update(member._id,member);
      }
    });
    return Events.remove(eventId);
  },
});
