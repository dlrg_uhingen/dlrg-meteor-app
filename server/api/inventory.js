// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Inventory } from '../../imports/collections/inventory.js';
import { permission, ucFirst } from '../../imports/functions.js';


Meteor.publish('inventory', ()=>{
  if(permission("inventory"))
    return Inventory.find();
});

Meteor.methods({
  'inventory.add'(name,amount) {
    if(!permission("inventory")) return;
    check(name, String);
    check(amount, String);
    if(!name)
      return "name is invalid";
    if(!amount)
      return "amount is invalid";

    return Inventory.insert({
      name: ucFirst(name),
      amount: amount,
      createdAt: new Date(),
    });
  },
  'inventory.setName'(inventoryId,name) {
    if(!permission("inventory")) return;
    check(inventoryId, String);
    check(name, String);
    if(!name)
      return "name is invalid";

    return Inventory.update(inventoryId,{ $set: {
      name
    } });
  },
  'inventory.setAmount'(inventoryId,amount) {
    if(!permission("inventory")) return;
    check(inventoryId, String);
    check(amount, String);
    if(!amount)
      return "amount is invalid";

    return Inventory.update(inventoryId,{ $set: {
      amount
    } });
  },
  'inventory.delete'(inventoryId) {
    if(!permission("deleteInventory")) return;
    check(inventoryId, String);

    return Inventory.remove(inventoryId);
  },
});
