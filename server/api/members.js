// All links-related publications
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Members } from '../../imports/collections/members.js';
import { Groups } from '../../imports/collections/groups.js';
import { Events } from '../../imports/collections/events.js';
import { permission, ucFirst } from '../../imports/functions.js';

Meteor.publish('members', ()=>{
  if(Meteor.userId()){
    return Members.find();
  }
});

Meteor.methods({
  'members.add'(prename,lastname) {
    if(!permission("addMember")) return;
    check(prename, String);
    check(lastname, String);
    if(!prename)
      return "prename is invalid";
    if(!lastname)
      return "lastname is invalid";

    return Members.insert({
      prename: ucFirst(prename),
      lastname: ucFirst(lastname),
      createdAt: new Date(),
    });
  },
  'members.setPrename'(memberId,prename) {
    if(!permission("editMember")) return;
    check(prename, String);
    return Members.update(memberId,{ $set: {
      prename
    } });
  },
  'members.setLastname'(memberId,lastname) {
    if(!permission("editMember")) return;
    check(lastname, String);
    return Members.update(memberId,{ $set: {
      lastname
    } });
  },
  'members.setGroups'(memberId,groupNames) {
    if(!permission("editMember")) return;
    check(memberId, String);
    check(groupNames, Array);

    let groupIds = [];
    Groups.find().forEach((group)=>{
      if(groupNames.indexOf(group.name) != -1){
        groupIds.push(group._id);
      }
    });

    if(groupIds.length == 0){
      return Members.update(memberId,{
        $unset: { groupIds: 1 }
      });
    }

    return Members.update(memberId,{ $set: {
      groupIds
    } });
  },
  'members.setAttendence'(memberId, eventId, attendence) {
    if(!permission("setAttendence") && !permission("editAttendence")) return;
    check(memberId, String);
    check(eventId, String);
    check(attendence, String);

    let member = Members.findOne(memberId);
    let selectedEvent = Events.findOne(eventId);

    if(member && selectedEvent){
        if(!member.attendence){
          member.attendence = {};
        }
        if(!member.attendence[eventId] || permission("editAttendence")){
          member.attendence[eventId] = attendence
        }

    }
    return Members.update(memberId,member);
  },
  'members.delete'(memberId) {
    if(!permission("deleteMember")) return;
    check(memberId, String);
    return Members.remove(memberId);
  },
});
