import { Meteor } from 'meteor/meteor';
import { Events } from '../imports/collections/events.js';
import { Members } from '../imports/collections/members.js';
import { Groups } from '../imports/collections/groups.js';

import { get_permissions } from '../imports/functions.js';

Meteor.startup(() => {
  console.log("Startup");
  if(!Meteor.users.findOne({username: "admin"})){
    console.log("create Admin");
    let password = Math.random().toString(36).substr(2);
    Accounts.createUser({
      username: "admin",
      password: password,
    });
    console.log("Password: "+password);
  }
  let permission = [];

  get_permissions().forEach((perm)=>{
    if(perm.key)
      permission.push(perm.key);
  });

  Meteor.users.update({username: "admin"},{
    $set:{
      permission
    }
  });
  console.log("Meteor is running");
});
