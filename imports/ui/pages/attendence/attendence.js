import './attendence.html';
import { ReactiveVar } from 'meteor/reactive-var'

import {permission, dateFormat, beginOfDay, endOfDay, beginOfYear} from '../../../functions.js';
import { Members } from '../../../collections/members.js';
import { Events } from '../../../collections/events.js';
import { Groups } from '../../../collections/groups.js';

let selectedEventId = new ReactiveVar();
let selectedGroupId = new ReactiveVar();
let searchFilter = new ReactiveVar();


Template.attendence.onRendered(()=>{
  if(FlowRouter.getParam('groupId')){
    selectedEventId.set(FlowRouter.getParam('groupId'));
  }else if(!selectedEventId.get()){
    let eventOfToday = Events.findOne({
      date: {
        $gte: beginOfDay(),
        $lte: endOfDay()
      }
    });
    if(eventOfToday){
      selectedEventId.set(eventOfToday._id);
    }
  }
});

Template.attendence.events({
  'change #eventId'(){
    let eventId = $("#eventId").val();
    selectedEventId.set(eventId);
  },
  'change #groupId'(){
    let groupId = $("#groupId").val();
    selectedGroupId.set(groupId);
  },
  'change .attendence'(event){
    let memberId = $(event.target).attr("name");
    let attendence = $(event.target).val();
    Meteor.call("members.setAttendence", memberId, selectedEventId.get(), attendence);
  },
  'keyup #search'(){
    searchFilter.set($("#search").val());
  }
});

Template.attendence.helpers({
  get_events(){
    return Events.find({
      date: {
        $gte: beginOfYear(),
        $lte: endOfDay(7)
      }
    },{
      sort:{
        date: -1
      }
    });
  },
  get_groups(){
    return Groups.find({},{
      sort:{
        name: 1
      }
    });
  },
  get_members(){
    if(selectedGroupId.get()){
      var cMembers = Members.find({
        groupIds: { $all: [selectedGroupId.get()] }
      },{
        sort: {
          lastname: 1,
          prename: 1
        }
      });
    }else{
      var cMembers = Members.find({},{
        sort: {
          lastname: 1,
          prename: 1
        }
      })
    }
    let members = [];
    cMembers.forEach((member)=>{
      let name = member.prename + " " + member.lastname;
      if(!searchFilter.get() || name.search(RegExp(searchFilter.get(),"i")) != -1){
        members.push(member);
      }
    });
    return members;
  },
  selectedEventId(){
    return selectedEventId.get()
  },
  selectedGroupId(){
    return selectedGroupId.get()
  },
  searchFilter(){
    return searchFilter.get()
  },
  checked_if_present(attendence){
    if(attendence && attendence[selectedEventId.get()] && attendence[selectedEventId.get()]=="present")
    return "checked"
  },
  checked_if_excused(attendence){
    if(attendence && attendence[selectedEventId.get()] && attendence[selectedEventId.get()]=="excused")
    return "checked"
  },
  checked_if_missing(attendence){
    if(attendence && attendence[selectedEventId.get()] && attendence[selectedEventId.get()]=="missing")
    return "checked"
  },
  disabled_by_permission(attendence){
    if(permission("editAttendence")){
      return "";
    }
    if(permission("setAttendence") && !(attendence && attendence[selectedEventId.get()])){
      return "";
    }
    return "disabled";
  }
});
