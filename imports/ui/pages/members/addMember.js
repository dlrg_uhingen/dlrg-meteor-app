import './addMember.html';
import { Groups } from '../../../collections/groups.js';

Template.addMember.onRendered(()=>{
  $('#add_member').modal();

  Tracker.autorun(()=>{
    let groupNames = [];
    Groups.find({},{
      sort:{ name: 1 }
    }).forEach((group)=>{
      groupNames[group.name] = 0;
    });
    M.Chips.init($('#groups'),{
      data: [],
      autocompleteOptions: {
        data: groupNames,
        limit: Infinity,
        minLength: 0
      }
    });
  });

});

Template.addMember.events({
  'click #open_add_member'(){
    $('#add_member').modal("open");
    $('#groupId').formSelect();
    reset_inputs();
  },
  'click #add_member_abord'(){
    reset_inputs();
    $('#add_member').modal("close");
  },
  'click #add_member_submit'(){
    let prename = $('#prename').val();
    let lastname = $('#lastname').val();
    let groupId = $('#groupId').val();

    if(!prename){
      $('#prename').addClass("invalid");
    }else if(!lastname){
      $('#lastname').addClass("invalid");
    }else{
      Meteor.call('members.add',prename,lastname,function(err,res){
          let groupNames = [];
          M.Chips.getInstance($('#groups')).chipsData.forEach((group)=>{
            groupNames.push(group.tag);
          });
          Meteor.call('members.setGroups',res,groupNames);
          $('#add_member').modal("close");
      });
      reset_inputs();
    }
  }
});

function reset_inputs(){
  $('#prename').val("").removeClass("invalid");
  $('#lastname').val("").removeClass("invalid");
  $('#groupId').val("no_group").formSelect("destroy");
  $('#groupId').formSelect();
  M.updateTextFields();
}

Template.addMember.helpers({

});
