import './members.html';
import { Members } from '../../../collections/members.js';
import { Groups } from '../../../collections/groups.js';

import "./addMember.js";

Template.members.onRendered(()=>{

});

Template.members.events({

});

Template.members.helpers({
  get_members(){
    return Members.find({},{
      sort:{
        lastname: 1,
        prename: 1
      }
    });
  },
  groupname(groupIds){
    let groupNames = []
    if(!groupIds) return "";
    groupIds.forEach((groupId)=>{
      let group = Groups.findOne(groupId);
      if(group){
         groupNames.push(group.name);
      }
    });
    groupNames.sort();
    return "("+groupNames.join(", ")+")";
  }
});
