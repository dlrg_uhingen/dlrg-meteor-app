import './editMember.html';
import { Members } from '../../../collections/members.js';
import { Groups } from '../../../collections/groups.js';

import '../../components/deleteButton/deleteButton.js';

Template.editMember.onRendered(()=>{
  Tracker.autorun(()=>{
    let member = Members.findOne(FlowRouter.getParam('memberId'));
    let groupNamesOfMember = [];
    let groupNames = [];

    if(!member) return;

    Groups.find({},{
      sort:{ name: 1 }
    }).forEach((group)=>{
      groupNames[group.name] = 0;
      if(member.groupIds && member.groupIds.indexOf(group._id) != -1){
        groupNamesOfMember.push({tag: group.name});
      }
    });

    function update_groups(){
      let groupNames = [];
      M.Chips.getInstance($('#groups')).chipsData.forEach((group)=>{
        groupNames.push(group.tag);
      });
      Meteor.call('members.setGroups',FlowRouter.getParam('memberId'),groupNames);
    }

    M.Chips.init($('#groups'),{
      data: groupNamesOfMember,
      autocompleteOptions: {
        data: groupNames,
        limit: Infinity,
        minLength: 0
      },
      onChipAdd: update_groups,
      onChipDelete: update_groups
    });
  });
});
Template.editMember.events({
  'change #prename'(){
    Meteor.call('members.setPrename',FlowRouter.getParam('memberId'),$('#prename').val());
  },
  'change #lastname'(){
    Meteor.call('members.setLastname',FlowRouter.getParam('memberId'),$('#lastname').val());
  }
});

Template.editMember.helpers({
  get_member(){
    let memberId = FlowRouter.getParam('memberId');
    if(memberId)
    return Members.findOne(memberId);
  }
});
