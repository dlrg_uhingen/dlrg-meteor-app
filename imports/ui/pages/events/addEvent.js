import './addEvent.html';
import {datepickerOptions,timepickerOptions,nextWeekday} from '../../../functions.js';

Template.addEvent.onRendered(()=>{
  $('#add_events').modal();
  $('.datepicker').datepicker(datepickerOptions({
    defaultDate: nextWeekday(1),
    setDefaultDate: true,
    minDate: new Date()
  }));
  $('.timepicker').timepicker(timepickerOptions({
    defaultTime: "18:45"
  }));
});

Template.addEvent.events({
  'click #open_add_events'(){
    $('#add_events').modal("open");
    reset_inputs();
  },
  'click #add_events_abord'(){
    reset_inputs();
    $('#add_events').modal("close");
  },
  'click #add_events_submit'(){
    let title = $('#title').val();
    let date = $('#date').val();
    let time = $('#time').val();

    if(!title){
      $('#title').addClass("invalid");
    }else if(!date){
      $('#date').addClass("invalid");
    }else if(!time){
      $('#time').addClass("invalid");
    }else{
      let datepicker = M.Datepicker.getInstance($("#date"));
      let timepicker = M.Timepicker.getInstance($("#time"));

      let time = timepicker.time.split(":");
      datepicker.date.setHours(time.shift());
      datepicker.date.setMinutes(time.shift());

      Meteor.call('events.add',title,datepicker.date,function(err,res){
          $('#add_events').modal("close");
      });
      reset_inputs();
    }
  }
});

function reset_inputs(){
  $('#title').val("Training").removeClass("invalid");
  $('#date').val("").removeClass("invalid");
  $('#time').val("").removeClass("invalid");
  M.updateTextFields();
}

Template.addEvent.helpers({

});
