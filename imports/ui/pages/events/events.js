import './events.html';
import './addEvent.js';
import { Events } from '../../../collections/events.js';

Template.events.onRendered(()=>{

});

Template.events.events({

});

Template.events.helpers({
  get_events(){
    return Events.find({},{
      sort:{
        date: -1
      }
    });
  }
});
