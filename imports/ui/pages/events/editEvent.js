import './editEvent.html';
import {ucFirst,dateFormat,datepickerOptions,timepickerOptions} from '../../../functions.js';

import '../../components/deleteButton/deleteButton.js';
import { Events } from '../../../collections/events.js';

function dateOfEvent(){
  let current_event = Events.findOne(FlowRouter.getParam('eventId'));
  if(current_event && current_event.date){
    return current_event.date;
  }
  return new Date();
}

function timeOfEvent(){
  let date = dateOfEvent();
  return dateFormat(date,"HH:MM");
}

Template.editEvent.onRendered(()=>{

  M.Datepicker.init($('#date'),datepickerOptions());
  M.Timepicker.init($('#time'),timepickerOptions());

  Tracker.autorun(()=>{
    if($("#date").length && M.Datepicker.getInstance($("#date")) && $("#time").length && M.Timepicker.getInstance($("#time"))){
      M.Datepicker.getInstance($("#date")).setDate(dateOfEvent());
      M.Timepicker.getInstance($("#time")).time = timeOfEvent();
    }
  });

});

Template.editEvent.events({
  'change #title'(){
    Meteor.call('events.setTitle',FlowRouter.getParam('eventId'),$('#title').val());
  },
  'change #date,#time'(){
    let datepicker = M.Datepicker.getInstance($("#date"));
    let timepicker = M.Timepicker.getInstance($("#time"));

    let time = timepicker.time.split(":");
    datepicker.date.setHours(time.shift());
    datepicker.date.setMinutes(time.shift());

    Meteor.call('events.setDate',FlowRouter.getParam('eventId'),datepicker.date);
  }
});

Template.editEvent.helpers({
  get_event(){
    return Events.findOne(FlowRouter.getParam('eventId'));
  }
});
