import './editGroup.html';
import { Groups } from '../../../collections/groups.js';

import '../../components/deleteButton/deleteButton.js';

Template.editGroup.onRendered(()=>{

});

Template.editGroup.events({
  'change #group_name'(){
    Meteor.call('groups.setName',FlowRouter.getParam('groupId'),$('#group_name').val());
  }
});

Template.editGroup.helpers({
  get_group(){
    let groupId = FlowRouter.getParam('groupId');
    if(groupId)
    return Groups.findOne(groupId);
  }
});
