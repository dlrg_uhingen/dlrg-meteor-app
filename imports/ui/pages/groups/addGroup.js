import './addGroup.html';

Template.addGroup.onRendered(()=>{
  $('#add_group').modal();
});

Template.addGroup.events({
  'click #add_group_abord'(){
    reset_inputs();
  },
  'click #add_group_submit'(){
    let name = $('#group_name').val();
    if(!name){
      $('#group_name').addClass("invalid");
    }else{
      Meteor.call('groups.add',name,function(err,res){
          reset_inputs();
      });
    }
  }
});

function reset_inputs(){
  $('#group_name').val("").removeClass("invalid");
  M.updateTextFields();
  $('#add_group').modal("close");
}

Template.addGroup.helpers({

});
