import './groups.html';
import { Groups } from '../../../collections/groups.js';

import './addGroup.js';


Template.groups.onRendered(()=>{

});

Template.groups.events({

});

Template.groups.helpers({
  get_groups(){
    return Groups.find({},{
      sort:{
        name: 1
      }
    });
  }
});
