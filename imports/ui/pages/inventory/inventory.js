import './inventory.html';
import { Inventory } from '../../../collections/inventory.js';

import './addInventory.js';

Template.inventory.onRendered(()=>{

});

Template.inventory.events({

});

Template.inventory.helpers({
  get_inventory(){
    return Inventory.find({},{
      sort:{
        name: 1
      }
    });
  }
});
