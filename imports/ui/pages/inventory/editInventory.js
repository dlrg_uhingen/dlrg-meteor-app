import './editInventory.html';
import { Inventory } from '../../../collections/inventory.js';

import '../../components/deleteButton/deleteButton.js';

Template.editInventory.onRendered(()=>{

});

Template.editInventory.events({
  'change #inventory_name'(){
    Meteor.call('inventory.setName',FlowRouter.getParam('inventoryId'),$('#inventory_name').val());
  },
  'change #inventory_amount'(){
    Meteor.call('inventory.setAmount',FlowRouter.getParam('inventoryId'),$('#inventory_amount').val());
  }
});

Template.editInventory.helpers({
  get_object(){
    let inventoryId = FlowRouter.getParam('inventoryId');
    if(inventoryId)
    return Inventory.findOne(inventoryId);
  }
});
