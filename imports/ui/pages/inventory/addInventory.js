import './addInventory.html';

Template.addInventory.onRendered(()=>{
  $('#add_inventory').modal();
});

Template.addInventory.events({
  'click #add_inventory_abord'(){
    reset_inputs();
    $('#add_inventory').modal("close");
  },
  'click #add_inventory_submit'(){
    let name = $('#inventory_name').val();
    let amount = $('#inventory_amount').val();
    if(!name){
      $('#inventory_name').addClass("invalid");
    }else if(!amount){
      $('#inventory_amount').addClass("invalid");
    }else{
      Meteor.call('inventory.add',name,amount,function(err,res){
          $('#add_inventory').modal("close");
      });
      reset_inputs();
    }
  }
});

function reset_inputs(){
  $('#inventory_name').val("").removeClass("invalid");
  $('#inventory_amount').val("").removeClass("invalid");
  M.updateTextFields();
}

Template.addInventory.helpers({

});
