import './editUser.html';

import '../../components/deleteButton/deleteButton.js';
import {permission,get_permissions} from '../../../functions.js';

Template.editUser.onRendered(()=>{

});

Template.editUser.events({
  'change #username'(){
    Meteor.call('users.setUsername',FlowRouter.getParam('userId'),$('#username').val(),(res)=>{
      if(res == "username_exists"){
        M.toast({html:"Benutzername existiert bereits"});
      }
      if(res == "username_empty"){
        M.toast({html:"Benutzername darf nicht leer sein"});
      }
    });
  },
  'click #changePassword'(){
    let currentPassword = $('#currentPassword').val();
    let newPassword = $('#newPassword').val();
    let newPassword2 = $('#newPassword2').val();
    if (newPassword != newPassword2){
      M.toast({html:"Neue Passwörter stimmen nicht überein"});
    }else{
      Accounts.changePassword(currentPassword, newPassword, (err)=>{
        if(err){
          M.toast({html:err.reason});
        }else{
          M.toast({html:"Passwort geändert"});
          $('#currentPassword').val("");
          $('#newPassword').val("");
          $('#newPassword2').val("");
        }
      });
    }
  },
  'change .permission'(e){
    let permissions = [];
    document.querySelectorAll('.permission:checked').forEach((node)=>{
        permissions.push(node.value);
    });
    Meteor.call('users.setPermission',FlowRouter.getParam('userId'),permissions,(res)=>{
      if(res == "self_editing_not_allowed"){
        M.toast({html: "Du kannst deine Berechtigungen nicht bearbeiten"});
      }
      if(res == "not_enought_permissions"){
        M.toast({html: "Du hast die Berechtigungen nicht"});
      }
    })
  }
});

Template.editUser.helpers({
  get_user(){
    return Meteor.users.findOne(FlowRouter.getParam('userId'));
  },
  deletePermission(){
    return permission("deleteUser") || Meteor.userId() == FlowRouter.getParam('userId')
  },
  get_permissions(){

    let permissions = get_permissions();

    let user = Meteor.users.findOne(FlowRouter.getParam('userId'));
    if(!user) return;

    for(n in permissions){
      if(user.permission && user.permission.indexOf(permissions[n].key) != -1){
        permissions[n].checked = 'checked';
      }
      if(!permission("editUserPermissons") || user._id == Meteor.userId()){
        permissions[n].disabled = 'disabled';
      }
    }
    return permissions;
  }
});
