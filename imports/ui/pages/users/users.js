import './users.html';
import './addUser.js';

Template.users.onRendered(()=>{

});

Template.users.events({

});

Template.users.helpers({
  get_users(){
    return Meteor.users.find({},{
      sort:{
        username: 1,
      }
    });
  },
});
