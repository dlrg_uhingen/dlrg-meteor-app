import './addUser.html';


Template.addUser.onRendered(()=>{
  $('#add_user').modal();
});

Template.addUser.events({
  'click #open_add_user'(){
    $('#add_user').modal("open");
    reset_inputs();
  },
  'click #add_user_abord'(){
    reset_inputs();
    $('#add_user').modal("close");
  },
  'click #add_user_submit'(){
    let username = $('#username').val();
    let password = $('#password').val();

    if(!username){
      $('#username').addClass("invalid");
    }else if(!password){
      $('#password').addClass("invalid");
    }else{
      Meteor.call('users.add',username,password,function(err,res){
        $('#add_user').modal("close");
        reset_inputs();
        if(res == "username_exists"){
          M.toast({html:"Benutzername existiert bereits"});
        }else if(res == "username_empty"){
          M.toast({html:"Benutzername darf nicht leer sein"});
        }else if(!err && res){
          FlowRouter.go("/users/edit/"+res);
        }
      });

    }
  }
});

function reset_inputs(){
  $('#username').val("").removeClass("invalid");
  $('#password').val("").removeClass("invalid");
  M.updateTextFields();
}

Template.users.helpers({

});
