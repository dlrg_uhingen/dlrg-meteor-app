import './home.html';

import {permission, beginOfDay, endOfDay, beginOfYear} from '../../../functions.js';
import { Events } from '../../../collections/events.js';
import { Members } from '../../../collections/members.js';


Template.home.onRendered(()=>{

});

Template.home.events({

});

Template.home.helpers({
  events_today(){
    return Events.find({
      date: {
        $gte: beginOfDay(),
        $lte: endOfDay()
      }
    });
  },
  get_members(){
    return Members.find();
  },
  get_events(){
    return Events.find();
  },
  average_attendence(status){

    let passedEvents = Events.find({
      date: {
        $lte: endOfDay()
      }
    });

    let sum = 0;
    Members.find().forEach((member)=>{
      for (var k in member.attendence) {
          if (member.attendence[k] == status) {
             sum++;
          }
      }
    });

    let average = sum / passedEvents.count();
    return Math.round(average);
  },
  most_attendence(status){

    let eventIdsThisYear = [];
    Events.find({
      date: {
        $gte: beginOfYear(),
        $lte: endOfDay()
      }
    }).forEach((event)=>{
      eventIdsThisYear.push(event._id);
    });

    let members = [];
    Members.find().forEach((member)=>{
      let count = 0;
      for (var k in member.attendence) {
          if (eventIdsThisYear.indexOf(k) != -1 && member.attendence[k] == status) {
             count++;
          }
      }
      member.attendence = count;
      members.push(member);
    });

    members.sort((a,b)=>{
      return b.attendence - a.attendence;
    });

    return members.slice(0, 15);
  }
});
