import './body.html';
import '../../components/nav/nav.js';
import '../../components/login/login.js';

Template.App_body.onRendered(function(){

});

Template.App_body.events({

});

Template.App_body.helpers({
  userId(){
    return Meteor.userId();
  },
  subscriptionReady(){
    return Meteor.users.find().count() > 0;
  }
});
