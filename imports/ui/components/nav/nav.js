import './nav.html';

import { permission } from '../../../functions.js';

Template.nav.onRendered(function(){
  $('.sidenav').sidenav();
  $('.dropdown-trigger').dropdown({
    coverTrigger: false,
    alignment: 'right',
    constrainWidth: false
  });
});

Template.nav.events({
  'click #logout'(){
    Meteor.logout();
  },
  'click .sidenav a'(){
    $('.sidenav').sidenav('close');
  }
});

Template.nav.helpers({
  pages() {
    let pages = [
      {
        title: "Anwesenheit",
        route: "/attendence",
        active: FlowRouter.getRouteName() == "attendence" ? "active" : "",
      },
      {
        title: "Termine",
        route: "/events",
        active: FlowRouter.getRouteName() == "events" ? "active" : "",
      },
      {
        title: "Mitglieder",
        route: "/members",
        active: FlowRouter.getRouteName() == "members" ? "active" : "",
      },
      {
        title: "Gruppen",
        route: "/groups",
        active: FlowRouter.getRouteName() == "groups" ? "active" : "",
      }
    ];

    if(permission("inventory")){
      pages.push({
        title: "Inventar",
        route: "/inventory",
        active: FlowRouter.getRouteName() == "inventory" ? "active" : "",
      });
    }

    if(permission("diary")){
      pages.push({
        title: "Tagebuch",
        route: "/diary",
        active: FlowRouter.getRouteName() == "diary" ? "active" : "",
      });
    }

    return pages;
  }
});
