import './deleteButton.html';

Template.deleteButton.onRendered(()=>{
  $('#deleteModal').modal();
});

Template.deleteButton.events({
  'click #deleteButtonSubmit'(){
    Meteor.call(this.collection+'.delete',this.id,()=>{
      $('#deleteModal').modal('close');
      M.toast({html:this.name+" gelöscht"});
      FlowRouter.go(this.collection);
    });
  }
});

Template.deleteButton.helpers({

});
