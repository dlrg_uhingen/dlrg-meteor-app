import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import layout
import '../imports/ui/layouts/body/body.js';

//import Pages
import '../imports/ui/pages/home/home.js';
import '../imports/ui/pages/members/members.js';
import '../imports/ui/pages/members/editMember.js';
import '../imports/ui/pages/groups/groups.js';
import '../imports/ui/pages/groups/editGroup.js';
import '../imports/ui/pages/attendence/attendence.js';
import '../imports/ui/pages/events/events.js';
import '../imports/ui/pages/events/editEvent.js';
import '../imports/ui/pages/inventory/inventory.js';
import '../imports/ui/pages/inventory/editInventory.js';
import '../imports/ui/pages/users/users.js';
import '../imports/ui/pages/users/editUser.js';
import '../imports/ui/pages/notFound/notFound.js';


FlowRouter.route('/', {
  name: 'home',
  action() {
    BlazeLayout.render('App_body', { page: 'home' });
  },
});

FlowRouter.route('/attendence', {
  name: 'attendence',
  action() {
    BlazeLayout.render('App_body', { page: 'attendence' });
  },
});

FlowRouter.route('/attendence/:groupId', {
  name: 'attendence',
  action() {
    BlazeLayout.render('App_body', { page: 'attendence' });
  },
});

FlowRouter.route('/events', {
  name: 'events',
  action() {
    BlazeLayout.render('App_body', { page: 'events' });
  },
});

FlowRouter.route('/events/edit/:eventId', {
  name: 'editEvent',
  action() {
    BlazeLayout.render('App_body', { page: 'editEvent' });
  },
});

FlowRouter.route('/members', {
  name: 'members',
  action() {
    BlazeLayout.render('App_body', { page: 'members' });
  },
});

FlowRouter.route('/members/edit/:memberId', {
  name: 'editMember',
  action() {
    BlazeLayout.render('App_body', { page: 'editMember' });
  },
});

FlowRouter.route('/groups', {
  name: 'groups',
  action() {
    BlazeLayout.render('App_body', { page: 'groups' });
  },
});

FlowRouter.route('/groups/edit/:groupId', {
  name: 'editGroup',
  action() {
    BlazeLayout.render('App_body', { page: 'editGroup' });
  },
});

FlowRouter.route('/inventory', {
  name: 'inventory',
  action() {
    BlazeLayout.render('App_body', { page: 'inventory' });
  },
});

FlowRouter.route('/inventory/edit/:inventoryId', {
  name: 'editInventory',
  action() {
    BlazeLayout.render('App_body', { page: 'editInventory' });
  },
});

FlowRouter.route('/users', {
  name: 'users',
  action() {
    BlazeLayout.render('App_body', { page: 'users' });
  },
});

FlowRouter.route('/users/edit/:userId', {
  name: 'editUser',
  action() {
    BlazeLayout.render('App_body', { page: 'editUser' });
  },
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { page: 'notFound' });
  },
};
