import {permission,dateFormat} from '../imports/functions.js';

Template.registerHelper('equal', (a,b) => {
  return a == b;
});

Template.registerHelper('selected_if_equal', (a,b) => {
  return a == b ? "selected" : "";
});

Template.registerHelper('permission', (key) => {
  return permission(key);
});

Template.registerHelper('the_date', (date) => {
  return dateFormat(date,dateFormat.masks.longDate);
});
Template.registerHelper('the_time', (date) => {
  return dateFormat(date,dateFormat.masks.time);
});
Template.registerHelper('the_dateTime', (date) => {
  return dateFormat(date,dateFormat.masks.dateTime);
});
Template.registerHelper('count', (cursor) => {
  return cursor.count();
});
